import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { COLORS } from '../../configs/color'
import Title from '../../components/Info/Title'
import OrderItem from '../../components/Order/OrderItem'
import { get } from 'lodash'
import { DATA_ORDERS } from '../../configs/data-order'

interface IOrder {
    title: string
    type: string
}

const color = COLORS


const Order = (params: IOrder) => {
    const [data, setData] = useState<any>()


    useEffect(() => {
        const order = params.type === "progress" ? DATA_ORDERS?.filter((item: any) => { return item.status == "progress" }) : DATA_ORDERS
        setData(order)
    })

    const status = {
        done: "green",
        progress: "orange",
        cancel: "red"

    }
    return (
        <View style={styles.container}>
            <Title title={params.title} />
            <View style={styles.list}>
                <ScrollView showsVerticalScrollIndicator={true}>
                    {data?.map((item: any, index: number) =>
                        <OrderItem
                            key={index}
                            id={item?.id}
                            name={item?.name}
                            status={item?.status}
                            color={get(status, item?.status)}
                            price={item.price}
                        />
                    )}
                </ScrollView>
            </View>
        </View>
    )
}

export default Order

const styles = StyleSheet.create({
    container: {
        backgroundColor: color.gray,
        borderRadius: 10,
        paddingHorizontal: 30,
        paddingTop: 10
    },
    list: {
        marginTop: 10,
        minHeight: 100
    }
})