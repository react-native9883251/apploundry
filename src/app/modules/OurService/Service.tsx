import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Button from '../../components/Button/Button'
import { COLORS } from '../../configs/color';
import Title from '../../components/Info/Title';

const colors = COLORS

const Service = () => {
    return (

        <View style={styles.layanan}>
            <Title title='Our Services' />
            {/* <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            > */}
                <View style={styles.containerService}>
                    <View style={styles.iconService}>
                        <Button icon='scales' label='Scales' />
                    </View>
                    <View style={styles.iconService}>
                        <Button icon='tShirt' label='Unit' />
                    </View>
                    <View style={styles.iconService}>
                        <Button icon='crown' label='VIP' />
                    </View>

                    <View style={styles.iconService}>
                        <Button icon='carpet' label='Carpet' />
                    </View>
                    <View style={styles.iconService}>
                        <Button icon='iron' label='Iron' />
                    </View>
                    <View style={styles.iconService}>
                        <Button icon='express' label='Express' />
                    </View>
                </View>
            {/* </ScrollView> */}

        </View>
    )
}

export default Service

const styles = StyleSheet.create({
    layanan: {
        paddingHorizontal: 30,
        paddingTop: 30
    },
    containerService: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 15,
        flexWrap: 'wrap'
    },
    iconService: {
        paddingHorizontal: 20,
        paddingVertical:10,
        backgroundColor: colors.secondary,
        marginEnd: 10,
        marginBottom: 10,
        borderRadius:15
    }
})