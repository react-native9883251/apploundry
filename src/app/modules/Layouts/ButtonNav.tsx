import { StyleSheet, View } from 'react-native'
import React from 'react'
import TabItemsNav from '../../components/Tabs/TabItemsNav';

const ButtonNav = ({
    state, descriptors, navigation
}:any) => {
    return (
        <View style={styles.container}>
            {state?.routes.map((route:any, index:number) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name, route.params);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                return (
                        <TabItemsNav
                            key={index}
                            isFocused={isFocused}
                            onPress={onPress}
                            onLongPress={onLongPress}
                            options={options}
                            label={label}
                        />
                );
            })}
        </View>
    )
}

export default ButtonNav

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        paddingHorizontal: 55,
        paddingVertical: 14,
    }
})