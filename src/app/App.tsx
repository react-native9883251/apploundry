import { StyleSheet, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Route from './routes/index';


const App = () => {
  return (
    // <View style={styles.body}>
      <NavigationContainer>
        <Route />
      </NavigationContainer>
    // </View>
  )
}

export default App

const styles = StyleSheet.create({
  body: {
    backgroundColor: "#fff"
  }
})