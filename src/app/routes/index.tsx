import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import HomeScreen from '../pages/Home/HomeScreen'
import ProfileScreen from '../pages/Profile/ProfileScreen'

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SplashScreen from '../pages/Splash/SplashScreen';
import { ButtonNav } from '../modules';
import OrderScreen from '../pages/Order/OrderScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator()

const MainApp = () => {
    return (
        <>
            <Tab.Navigator tabBar={props => <ButtonNav {...props} />}>
                <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }}/>
                <Stack.Screen name="Order" component={OrderScreen} />
                <Stack.Screen name="Profile" component={ProfileScreen} />
            </Tab.Navigator>
        </>
    )
}

const index = () => {
    return (
        <>
            <Stack.Navigator initialRouteName='Splash'>
                <Stack.Screen name="MainApp" component={MainApp}  options={{ headerShown: false }}/>
                <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }}/>
            </Stack.Navigator>
        </>
    )
}

export default index

const styles = StyleSheet.create({})