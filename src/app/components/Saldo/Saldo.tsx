import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { formatThousand, numberCurrencyID } from '../../helpers/number.helper'
import { COLORS } from '../../configs/color'
import Button from '../Button/Button'

const colors = COLORS
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Saldo = () => {


    const handleClick = () => {
        console.log("");

    }

    return (
        <View style={styles.container}>
                <View style={styles.infoSlado}>
                        <View style={styles.text}>
                            <Text style={styles.labelSaldo}>Saldo</Text>
                            <Text style={styles.valueSaldo}> {numberCurrencyID(10000)}</Text>
                        </View>
                        <View style={styles.text}>
                            <Text style={styles.labelPoint}>Point</Text>
                            <Text style={styles.valuePoint}>{formatThousand(10000)} Points</Text>
                        </View>
                </View>
                <View style={styles.buttonAction}>
                    <View>
                        <View style={styles.buttonActionAdd}>
                            <Button icon="moneyCheck" />
                        </View>
                        <View style={{ marginLeft: 3 }}>
                            <Text style={styles.buttonText}>Add Saldo</Text>
                        </View>
                    </View>
                    <View>
                        <View style={styles.buttonActionPoint}>
                            <Button icon="coin" />
                        </View>
                        <Text style={styles.buttonText}>Add Points</Text>
                    </View>
                </View>
        </View>
    )
}

export default Saldo


const styles = StyleSheet.create({
    container: {
        fontFamily: "poppin",
        backgroundColor: "#fff",
        borderRadius: 10,
        padding: 17,
        marginHorizontal: 30,
        marginTop: -windowHeight * 0.08,
        // height: 100,
        justifyContent: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        flexDirection: "row",

    },
    text: {
        flexDirection: "row",
        justifyContent: "space-between"

    },
    infoSlado: {
        // width: "60%",
        marginEnd: 10,
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        // alignItems: 'center'
    },
    labelSaldo: {
        fontSize: 20
    },
    valueSaldo: {
        fontSize: 20,
        fontWeight: "bold"
    },
    labelPoint: {
        fontSize: 12,
        color: colors.button_nav.active
    },
    valuePoint: {
        fontSize: 12,
        fontWeight: "bold"
    },
    buttonAction: {
        flexDirection: "row",
        justifyContent: "space-between",
        fontSize: 8
    },
    buttonActionAdd: {
        marginEnd: 10,
        padding: 8,
        borderRadius: 10,
        backgroundColor: colors.secondary,
        height: 55
    },
    buttonActionPoint: {
        padding: 8,
        alignItems: "center",
        height: 55,
        borderRadius: 10,
        backgroundColor: colors.secondary
    },
    buttonText: {
        fontSize: 10,
    },
})