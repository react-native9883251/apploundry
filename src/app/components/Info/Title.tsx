import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

interface ITitle {
    title: string
}

const Title = (params: ITitle) => {
    return (
        <View>
            <Text style={styles.label}>{params.title}</Text>
        </View>
    )
}

export default Title

const styles = StyleSheet.create({
    label: {
        fontSize: 18,
        fontWeight: 'bold'
    },
})