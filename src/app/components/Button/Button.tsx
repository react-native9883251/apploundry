import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMoneyCheckDollar } from '@fortawesome/free-solid-svg-icons/faMoneyCheckDollar'
import { faCoins } from '@fortawesome/free-solid-svg-icons/faCoins'
import { get } from "lodash"
import { faCrown, faRub, faRug, faScaleBalanced, faTShirt } from '@fortawesome/free-solid-svg-icons'
import { faTruckFast } from '@fortawesome/free-solid-svg-icons/faTruckFast'
import { faAccessibleIcon } from '@fortawesome/free-brands-svg-icons'

interface IButton {
    label?: string
    icon?: string
    click?: any
    styles?: any

}

const icons = {
    moneyCheck: <FontAwesomeIcon icon={faMoneyCheckDollar} color='green' size={40} />,
    coin: <FontAwesomeIcon icon={faCoins} color='orange' size={40} />,
    scales: <FontAwesomeIcon icon={faScaleBalanced} color='green' size={50} />,
    tShirt: <FontAwesomeIcon icon={faTShirt} color='green' size={50} />,
    express: <FontAwesomeIcon icon={faTruckFast} color='green' size={50} />,
    carpet: <FontAwesomeIcon icon={faRug} color='green' size={50} />,
    crown: <FontAwesomeIcon icon={faCrown} color='green' size={50} />,
    iron: <FontAwesomeIcon icon={faAccessibleIcon} color='green' size={50} />,
    orderActive: <View></View>,
}

const Button = ({
    label,
    click,
    icon,
}: IButton) => {
    return (
        <TouchableOpacity>
            <View>
                {icon ? get(icons, icon) : null}
                {label ?
                    <Text style={styles.label}>
                        {label}
                    </Text>
                    : null
                }
            </View>

        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    label:{
        textAlign:"center"
    }
})