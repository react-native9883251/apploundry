import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { IconAkun, IconAkunActive, IconHome, IconHomeActive, IconPesanan, IconPesananActive } from '../../../assets/icons'
import { COLORS } from '../../configs/color'

const TabItemsNav = ({
    onLongPress,
    onPress,
    label,
    isFocused,
    options
}: any) => {
    const color=COLORS;
    const Icon = () => {
        if (label == "Home") return isFocused ? <IconHomeActive /> : <IconHome />
        else if (label == "Order") return isFocused ? <IconPesananActive /> : <IconPesanan />
        else if (label == "Profile") return isFocused ? <IconAkunActive /> : <IconAkun />
        return <IconHome />
    }
    return (
        <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}

        >
            <Icon />
            <Text style={isFocused ? styles.textFocus: styles.text}>
                {label}
            </Text>
        </TouchableOpacity>
    )
}

export default TabItemsNav

const styles = StyleSheet.create({
    container:{
        alignItems:'center',

    },
    text:{
        fontSize:13,
        margin:8,
        color: COLORS.button_nav.disabled
    },

    textFocus:{
        fontSize:13,
        margin:8,
        color: COLORS.button_nav.active
    },
    

})