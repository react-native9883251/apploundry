import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { IconPesananAktif } from '../../../assets/icons'
import { numberCurrencyID } from '../../helpers/number.helper'

interface IOrderItem {
    status: string
    id: string
    name: string
    color:string
    price:number
}

const OrderItem = (params:IOrderItem) => {
    return (
        <TouchableOpacity style={styles.container}>
            <IconPesananAktif />
            <View style={styles.label}>
                <Text style={styles.name}>{params?.name}</Text>
                <Text style={{fontSize:12,color:params?.color}}>{params?.status}</Text>
                <Text style={{fontSize:12}}>{numberCurrencyID(params?.price)}</Text>

            </View>
            
        </TouchableOpacity>
    )
}

export default OrderItem

const styles = StyleSheet.create({
    container:{
        flexDirection:"row",
        borderRadius:10,
        backgroundColor:"#fff",
        height:80,
        marginBottom:10,
        padding:17,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 2,
    },
    label:{
        marginStart:10
    },
    name:{
        fontWeight:'800'
    }
})