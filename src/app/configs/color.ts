export const COLORS = {
    button_nav: {
        active: "#55CB95",
        disabled: "#C8C8C8"
    },
    secondary: "#E0F7EF",
    gray: "#F6F6F6",
    warning: "#FF4D00",
}