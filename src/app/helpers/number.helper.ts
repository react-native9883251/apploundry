export function numberCurrencyID(value: any) {
    return `Rp. ${(parseFloat(value) || 0).toLocaleString('id-ID')}`;
}
export function formatThousand(value: any) {
    return `${(parseFloat(value) || 0).toLocaleString('id-ID')}`;
  }