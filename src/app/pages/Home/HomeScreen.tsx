import { Dimensions, Image, ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { ImageHeader, Logo } from '../../../assets/images'
import { Saldo } from '../../components'
import Service from '../../modules/OurService/Service';
import Order from '../../modules/Order/Order';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const HomeScreen = () => {


  return (
    <ScrollView showsVerticalScrollIndicator={false} >
      <View style={styles.page}>
        <ImageBackground source={ImageHeader} style={styles.header}>
          <Image key={1} source={Logo} style={styles.logo} />
          <View style={styles.hello}>
            <Text key={4} style={styles.selamat}>Welcome</Text>
            <Text key={5} style={styles.username}>John Dee</Text>
          </View>
        </ImageBackground>
        <Saldo />
        <Service />
        <Order title='Inprogress Orders' type='progress'/>
      </View>
    </ScrollView>
  )
}

export default HomeScreen




const styles = StyleSheet.create({
  page: {
    flex: 1,
    fontFamily: "poppin",
   backgroundColor:"#fff",
   minHeight:"100%"
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.30,
    paddingHorizontal: 30,
    paddingTop: 10
  },

  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    // flex:1,
    margin: windowHeight * 0.03
  },
  selamat: {
    fontSize: 24,
  },
  username: {
    fontWeight: 'bold',
    fontSize: 18

  }
})